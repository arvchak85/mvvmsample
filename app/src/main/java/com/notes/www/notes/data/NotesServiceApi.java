/**
 * Legal Notice! DB Systel GmbH proprietary License!
 * 
 * Copyright (C) 2017 DB Systel GmbH
 * DB Systel GmbH; J�rgen-Ponto-Platz 1; D-60329 Frankfurt am Main; Germany; http://www.dbsystel.de/
 * 
 * This code is protected by copyright law and is the exclusive property of
 * DB Systel GmbH; J�rgen-Ponto-Platz 1; D-60329 Frankfurt am Main; Germany; http://www.dbsystel.de/
 * 
 * Consent to use ("licence") shall be granted solely on the basis of a
 * written licence agreement signed by the customer and DB Systel GmbH. Any
 * other use, in particular copying, redistribution, publication or
 * modification of this code without written permission of DB Systel GmbH is
 * expressly prohibited.
 * 
 * In the event of any permitted copying, redistribution or publication of
 * this code, no changes in or deletion of author attribution, trademark
 * legend or copyright notice shall be made.  
 */
package com.notes.www.notes.data;

import java.util.List;

public interface NotesServiceApi {
    interface NotesServiceCallback<T> {

        void onLoaded(T notes);
    }

    interface NotesSaveServiceCallback<T> {
        void onSaved(T note);
    }

    void getAllNotes(NotesServiceCallback<List<Note>> callback);

    void getNote(String noteId, NotesServiceCallback<Note> callback);

    void saveNote(Note note);
}
