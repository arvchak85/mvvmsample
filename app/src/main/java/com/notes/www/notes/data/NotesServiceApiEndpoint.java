/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.notes.www.notes.data;

import java.util.HashMap;
import java.util.Map;

public final class NotesServiceApiEndpoint {

    static {
        DATA = new HashMap<>(2);
        addNote("title1", "Notes1");
        addNote("Espresso", "Functional testing for android");
    }

    private final static Map<String, Note> DATA;

    private static void addNote(String title, String description) {
        Note newNote = new Note(title, description);
        DATA.put(newNote.getId(), newNote);
    }

    /**
     * @return the Notes to show when starting the app.
     */
    public static Map<String, Note> loadPersistedNotes() {
        return DATA;
    }
}
