package com.notes.www.notes.addnotes;


import android.content.Context;
import android.databinding.ObservableField;

import com.notes.www.notes.R;
import com.notes.www.notes.data.NoteRepository;
import com.notes.www.notes.data.Note;
import com.notes.www.notes.data.source.NoteDataSource;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ArvindChakravarthy on 16.05.2017.
 */

public class AddNoteViewModel {

    public final ObservableField<String> title = new ObservableField<>();
    public final ObservableField<String> notes = new ObservableField<>();
    public final ObservableField<String> snackbarText = new ObservableField<>();
    private final Context mContext;
    private final NoteRepository mAddNoteRepository;
    private boolean isNewNote;
    private String mNoteId;
    private AddNoteNavigator addNoteNavigator;


    public AddNoteViewModel(Context mContext, NoteRepository mAddNoteRepository) {
        this.mContext = checkNotNull(mContext);
        this.mAddNoteRepository = checkNotNull(mAddNoteRepository);
    }

    public void onActivityCreated(AddNoteNavigator addNoteNavigator) {

        this.addNoteNavigator = addNoteNavigator;
    }


    private boolean isNewNote() {
        return isNewNote;
    }

    public void onStart(String taskid) {
        mNoteId = taskid;
        if (taskid == null) {
            isNewNote = true;
            return;
        }
        isNewNote = false;
        mAddNoteRepository.retrieveNote(taskid, new NoteDataSource.GetNoteCallback() {
            @Override
            public void onNoteLoaded(Note note) {
                title.set(note.title);
                notes.set(note.notes);
            }

            @Override
            public void onNoteNotAvailable() {
                snackbarText.set(mContext.getString(R.string.data_not_available));
            }

        });
    }

    public void saveNote(String title, String notes) {
        if (isNewNote()) {
            createTask(title, notes);
        } else {
            updateTask(title, notes);
        }
    }

    private void updateTask(String title, String notes) {
        if (mAddNoteRepository.updateNote(new Note(title, notes, mNoteId))) {
            addNoteNavigator.onNoteSaved();
        } else {
            snackbarText.set(mContext.getString(R.string.update_note_unsuccessfull));
        }

    }

    private void createTask(String title, String notes) {

        Note note = new Note(title, notes);
        if (note.isEmpty()) {
            snackbarText.set(mContext.getString(R.string.empty_note_message));
        } else {
            if (mAddNoteRepository.createNote(note) != null) {
                snackbarText.set(mContext.getString(R.string.create_note_successfully));
                addNoteNavigator.onNoteSaved();
            } else {
                snackbarText.set(mContext.getString(R.string.create_note_unsuccessfull));
            }
        }


    }


    public void clearNotes() {
        title.set("");
        notes.set("");
    }
}
