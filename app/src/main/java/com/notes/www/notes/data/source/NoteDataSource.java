package com.notes.www.notes.data.source;

import android.support.annotation.NonNull;

import com.notes.www.notes.data.Note;

import java.util.List;

import static com.notes.www.notes.R.id.notes;

/**
 * Created by ArvindChakravarthy on 16.05.2017.
 */

public interface NoteDataSource {

    interface GetNoteCallback {

        void onNoteLoaded(Note note);

        void onNoteNotAvailable();
    }

    interface LoadNotesCallback

    {
        void onNotesLoaded(List<Note> notes);

        void onDataNotAvailable();

    }

    String createNote(@NonNull Note note);

    boolean updateNote(@NonNull Note note);

    void retrieveNote(@NonNull String noteId, @NonNull GetNoteCallback callback);

    void retrieveNotes(@NonNull LoadNotesCallback callback);

    void getNote(@NonNull LoadNotesCallback callback);

    boolean deleteNote(@NonNull String noteId);

    boolean deleteAllNotes();


}
