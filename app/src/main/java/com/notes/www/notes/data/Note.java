package com.notes.www.notes.data;

import java.util.UUID;

/**
 * Created by ArvindChakravarthy on 16.05.2017.
 */

public class Note {

    public String title;
    public String notes;
    private String mNoteId;

    public Note(String title, String notes) {
        this.mNoteId = UUID.randomUUID().toString();
        this.title = title;
        this.notes = notes;
    }

    public Note(String title, String notes, String mNoteId) {

        this.title = title;
        this.notes = notes;
        this.mNoteId = mNoteId;
    }

    public boolean isEmpty() {
        return (title == null || title.isEmpty()) && (notes == null || notes.isEmpty());
    }

    public String getId(){
        return mNoteId;
    }
}
