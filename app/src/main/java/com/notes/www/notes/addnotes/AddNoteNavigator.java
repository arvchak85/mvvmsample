package com.notes.www.notes.addnotes;

/**
 * Created by ArvindChakravarthy on 29.05.2017.
 */

public interface AddNoteNavigator {

    void onNoteSaved();
}
