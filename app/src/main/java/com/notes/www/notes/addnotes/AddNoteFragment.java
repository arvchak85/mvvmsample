package com.notes.www.notes.addnotes;

import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.notes.www.notes.R;
import com.notes.www.notes.data.NoteRepository;
import com.notes.www.notes.data.NotesServiceApiImpl;
import com.notes.www.notes.databinding.AddNotesBinding;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ArvindChakravarthy on 16.05.2017.
 */

public class AddNoteFragment extends Fragment implements AddNoteNavigator {

    private AddNotesBinding binding;
    private AddNoteViewModel viewModel;
    private Observable.OnPropertyChangedCallback snackbarPropertyChangeCallback;



    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null) {
            viewModel.onStart(getArguments().getString("noteid"));
        } else {
            viewModel.onStart(null);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel.onActivityCreated(this);
        setUpSaveButton();
        setUpSnackbar();

    }

    private void setUpSaveButton() {
        FloatingActionButton saveButton = (FloatingActionButton) getActivity().findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.saveNote(viewModel.title.get(), viewModel.notes.get());
            }
        });
    }

    private void setViewModel(@NonNull AddNoteViewModel viewModel) {
        this.viewModel = checkNotNull(viewModel);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (snackbarPropertyChangeCallback != null) {
            viewModel.snackbarText.removeOnPropertyChangedCallback(snackbarPropertyChangeCallback);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewModel = new AddNoteViewModel(getContext() , new NoteRepository(new NotesServiceApiImpl()));
        View root = inflater.inflate(R.layout.add_notes, container, false);
        if (binding == null) {
            binding = AddNotesBinding.bind(root);
        }
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    private void setUpSnackbar() {
        snackbarPropertyChangeCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                Snackbar.make(getView(), viewModel.snackbarText.get(), Snackbar.LENGTH_LONG).show();
            }
        };
        viewModel.snackbarText.addOnPropertyChangedCallback(snackbarPropertyChangeCallback);
    }

    public static AddNoteFragment newInstance() {
        return new AddNoteFragment();
    }

    @Override
    public void onNoteSaved() {
        viewModel.clearNotes();
    }
}
