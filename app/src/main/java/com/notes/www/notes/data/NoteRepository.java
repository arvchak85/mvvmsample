package com.notes.www.notes.data;

import android.support.annotation.NonNull;

import com.notes.www.notes.data.source.NoteDataSource;

import java.util.List;

/**
 * Created by ArvindChakravarthy on 16.05.2017.
 */

public class NoteRepository implements NoteDataSource {

    private final NotesServiceApi notesServiceApi;

    public NoteRepository(NotesServiceApi notesServiceApi) {
        this.notesServiceApi = notesServiceApi;
    }

    @Override
    public String createNote(@NonNull Note note) {
        notesServiceApi.saveNote(note);
        return note.getId();
    }

    @Override
    public boolean updateNote(@NonNull Note note) {
        notesServiceApi.saveNote(note);
        return true;
    }

    @Override
    public void retrieveNote(@NonNull String noteId, @NonNull final GetNoteCallback callback) {
        notesServiceApi.getNote(noteId, new NotesServiceApi.NotesServiceCallback<Note>() {
            @Override
            public void onLoaded(Note note) {
                callback.onNoteLoaded(note);
            }
        });
    }

    @Override
    public void retrieveNotes(@NonNull final LoadNotesCallback callback) {
        notesServiceApi.getAllNotes(new NotesServiceApi.NotesServiceCallback<List<Note>>() {
            @Override
            public void onLoaded(List<Note> notes) {
                callback.onNotesLoaded(notes);
            }
        });
    }

    @Override
    public void getNote(@NonNull LoadNotesCallback callback) {

    }

    @Override
    public boolean deleteNote(@NonNull String noteId) {
        return false;
    }

    @Override
    public boolean deleteAllNotes() {
        return false;
    }
}
